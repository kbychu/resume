[![Netlify Status](https://api.netlify.com/api/v1/badges/d034b3d8-8694-49d8-8d98-4d6a23426a8b/deploy-status)](https://app.netlify.com/sites/lucid-johnson-c1214b/deploys)

# What is this?

This project is Kevin Chu's resume based on the [Gridsome starter resume](https://github.com/LokeCarlsson/gridsome-starter-resume). It is hosted on [resume.kbychu.com](https://resume.kbychu.com).

### Getting started

1. Install the Gridsome CLI: `npm install --global @gridsome/cli`
1. `gridsome create resume https://gitlab.com/kbychu/resume.git`
1. `cd resume` to open folder
1. `yarn dev` to start local dev server at `http://localhost:8080`
1. 🎉 🙌