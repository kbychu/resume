module.exports = {
  siteName: 'kbychu resume',
  siteDescription: "kbychu's resume based on gridsome starter resume",
  siteUrl: 'https://resume.kbychu.com',
  plugins: [{
      use: '@gridsome/plugin-google-analytics',
      options: {
        id: 'G-8XPCFC5PDP'
      }
    },
    {
      use: '@gridsome/plugin-sitemap',
      options: {
        cacheTime: 600000
      }
    }
  ],
  css: {
    loaderOptions: {
      scss: {}
    }
  }
}